#if defined(__CINT__) || defined(__ROOTCLING__) || defined(__CLING__)

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Ndmspc::Axis + ;
#pragma link C++ class Ndmspc::Cuts + ;
#pragma link C++ class Ndmspc::Utils + ;
#pragma link C++ class Ndmspc::Core + ;
#pragma link C++ class Ndmspc::Results + ;
#pragma link C++ class Ndmspc::PointRun + ;
#pragma link C++ class Ndmspc::PointDraw + ;
#pragma link C++ class Ndmspc::HnSparseBrowser + ;

#endif
